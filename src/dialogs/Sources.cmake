set( krosswordpuzzle_SRCS ${krosswordpuzzle_SRCS}
   dialogs/crosswordtypewidget.cpp
   dialogs/crosswordpreviewwidget.cpp
   dialogs/answerwidget.cpp
   dialogs/createnewcrossworddialog.cpp
   dialogs/crosswordtypeconfiguredetailsdialog.cpp
   dialogs/movecellsdialog.cpp
   dialogs/crosswordpropertiesdialog.cpp
   dialogs/convertcrossworddialog.cpp
   dialogs/currentcellwidget.cpp
   dialogs/statisticsdialog.cpp
   dialogs/dictionarydialog.cpp
)